# MyBlog

#### 博客链接：[www.siriusshum.club](https://www.siriusshum.club)

该项目是基于SpringBoot开发的个人博客平台。虽然同样从事着java开发的工作，平时也积累了很多知识和经验，但好久都没写过和总结过什么。特意开发了一个个人博客以激励自己写些文章，真正的转化成自己可以灵活运用的技能，也方便日后查阅。

#### 导入项目
可在application.properties配置文件中修改数据库、redis等连接信息，并导入SQL文件建立本地数据库。

#### 关键字  
- Springboot
- 前后端分离(Thymeleaf、Vue、Axios)
- Redis
- Nginx
- ElasticSearch
- Shiro
- Jpa

## 项目设计

#### 总体设计
- **本项目用到的技术和框架**<br>
1.项目构建：Maven<br>
2.web框架：Springboot<br>
3.数据库ORM：Jpa<br>
4.数据库连接池： Druid<br>
5.分页插件：PageHelper<br>
6.数据库：MySql<br>
7.缓存：Redis<br>
8.前端模板：Thymeleaf<br>
9.前端框架：Vue、Axios<br>
10.搜索：ElasticSearch<br>
11.反向代理：Nginx<br>
12.安全框架：Shiro<br>

- **环境**

| 工具 | 名称 
| ------------ | ------------
| 开发工具  | IDEA 
| 语言 | JDK1.8、HTML、css、js 
| 数据库  | Mysql
| 项目框架  | SpringBoot 
| ORM  | Jpa 
| 安全框架  | Shiro 
| 缓存  | Redis 
| 项目构建  | Maven 
| 运行环境  | Linux 

#### 结构设计

```
├── README.md                       read_me
├── pom.cml                         maven依赖
├── .gitignore                      git忽略提交文件
├── doc                       		文档
│   ├── myblog.sql                  数据库导入文件
│   └── es_documents.txt            es文档索引
└── src                             源代码
    └── main                        
    	├── java/com/syw/myblog     java代码
    	    ├── comparator          比较器
    	    ├── config              配置文件目录
    	    ├── controller          控制器
    	    ├── dao                 数据库持久层
    	    ├── exception           异常文件目录
    	    ├── interception        拦截器
    	    ├── pojo                java bean
    	    ├── read                websocket-阅读记录
    	    ├── realm               权限认证
    	    ├── service             业务层
    	    ├── utils               工具类         
    	    └── MyblogApplication.java 启动类
        └── resources               资源文件
            ├── static              静态资源
            ├── templates           模板层
            ├── application.properties  应用配置文件
            ├── hosts.properties        主机配置文件
            └── log4j.properties        日志配置文件                 
```