package com.syw.myblog.dao;

import com.syw.myblog.pojo.FriendLink;
import org.springframework.data.jpa.repository.JpaRepository;

// jpa持久层
public interface FriendLinkDao extends JpaRepository<FriendLink,Integer> {

}
