package com.syw.myblog.dao;

import com.syw.myblog.pojo.SiteInfo;
import org.springframework.data.jpa.repository.JpaRepository;

// jpa持久层
public interface SiteInfoDao extends JpaRepository<SiteInfo,Integer> {

}
