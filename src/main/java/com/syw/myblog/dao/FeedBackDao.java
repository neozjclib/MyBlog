package com.syw.myblog.dao;

import com.syw.myblog.pojo.FeedBack;
import org.springframework.data.jpa.repository.JpaRepository;

// jpa持久层
public interface FeedBackDao extends JpaRepository<FeedBack,Integer> {

}
