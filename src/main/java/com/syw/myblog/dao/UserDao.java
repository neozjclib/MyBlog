package com.syw.myblog.dao;

import com.syw.myblog.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

// jpa持久层
public interface UserDao extends JpaRepository<User,Integer> {
    User findByUsername(String name);
    User getByUsernameAndPassword(String username, String password);
}
