package com.syw.myblog.service;

import com.syw.myblog.dao.SiteInfoDao;
import com.syw.myblog.pojo.SiteInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames="site_info")
public class SiteInfoService {
    private static final int siteId = 1;    // 站点信息id

    @Autowired
    SiteInfoDao siteInfoDao;

    /*
    * 获取站点信息
    * */
    @Cacheable(key="'siteinfo-one'")
    public SiteInfo findOne() {
        return siteInfoDao.getOne(siteId);
    }
}
